package com.github.homework_priority;

import java.time.LocalDateTime;


class HomeworkAssignment {

	// Fields
	private final String assignmentName;
	private final String className;
	private LocalDateTime dueDate;  //hours till due
	private int timeToComplete; //approx time necessary to complete in hours
	private int latePenalty; //points lost per day i.e. 10pts/day 100pt/day = no late work
	private int gradePercent;
	private int classImportance; //priority given to class, major required vs elective
	private int currentAverage;

	//Constructor
	HomeworkAssignment(String an, String cn, LocalDateTime dd, int tc,
				  int lp, int gp, int ci, int ca) {
		assignmentName  = an;
		className       = cn;
		dueDate         = dd;
		timeToComplete  = tc;
		latePenalty     = lp;
		gradePercent    = gp;
		classImportance = ci;
		currentAverage  = ca;
	}

	String assignmentName() {
		return assignmentName;
	}

	String className() {
		return className;
	}

	@Override
    public String toString() {
    	return String.format("Assignment: %s%nClass: %s%nDue Date: %s%nExpected Hours to Complete: %d%nPoints/day-late: %d%nPercent of Final Grade: %d%nClass Importance: %d%nCurrent Average: %d%n",
    	 assignmentName, className, dueDate, timeToComplete,
    	 latePenalty, gradePercent, classImportance, currentAverage);
    }

}
