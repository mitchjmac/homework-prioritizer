package com.github.homework_priority;

import java.util.ArrayList;
import java.util.Comparator;
import java.time.format.DateTimeParseException;
import java.time.LocalDateTime;
import java.io.InputStream;
import java.io.ByteArrayInputStream;


public class HomeworkPrioritizer { // inherit ArrayList?

	// Fields
	private final ArrayList<HomeworkAssignment> schedule;
	private final DataInput input;

	// Constructors
	HomeworkPrioritizer(InputStream inStream) {
		schedule = new ArrayList<HomeworkAssignment>();
		input = new DataInput(inStream);
	}

	// Methods
	boolean addAssignment() throws DateTimeParseException, NumberFormatException {
		String an        = input.getAssignmentName();
		String cn        = input.getClassName();
		LocalDateTime dd = input.getDueDate();
		int tc           = input.getTimeToComplete();
		int lp           = input.getLatePenalty();
		int gp           = input.getGradePercent();
		int ci           = input.getClassImportance();
		int ca           = input.getCurrentAverage();
		return schedule.add(new HomeworkAssignment(an, cn, dd, tc, lp, gp, ci, ca));
	}

	boolean removeAssignment() {
		String an = input.getAssignmentName();
		String cn = input.getClassName();
		return schedule.removeIf(a -> (a.assignmentName().equals(an) && a.className().equals(cn)));
	}

	void removeAll() {
		schedule.clear();
	}

	void printSchedule() {
		schedule.sort(Comparator.comparing((HomeworkAssignment a)->a.assignmentName())
          .thenComparing(a->a.className()));
		for (HomeworkAssignment a: schedule) {
			System.out.println(a.toString());
		}
	}


	public static void main(String[] args) {
		HomeworkPrioritizer hp = new HomeworkPrioritizer(System.in);
		DataInput in = new DataInput(System.in);
		int option;

		while(true) {
			switch(option = in.getOption()) {
				case 1: // Add New Homework Assignment to Schedule
					hp.addAssignment();
					break;
				case 2: // Remove Homework Assignment from Schedule
					hp.removeAssignment();
					break;
				case 3: // Remove all Homework Items from Schedule
					hp.removeAll();
					break;
				case 4: // Load Schedule
					break;
				case 5: // Save Schedule
					break;
				case 6: // Print Schedule to Standard Out
					hp.printSchedule();
					break;
				case 7: // exit
					System.exit(0);
				default: // bad input
					System.err.println("HomeworkPrioritizer: Undefined option: '" + option + "'");
			}
		}

	}

}










