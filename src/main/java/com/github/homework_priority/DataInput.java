package com.github.homework_priority;

import java.util.Scanner;
import java.time.format.DateTimeParseException;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.io.InputStream;


class DataInput { // inherit scanner?

	// Fields
	private final Scanner sc;

	// Constructor
	DataInput(InputStream inStream) {
		sc = new Scanner(inStream);
	}

	// Methods
	String getAssignmentName() {
		String prompt = "Assignment Name: ";
		return readString(prompt);
	}

	int getClassImportance() throws NumberFormatException {
		int toReturn;
		String prompt = "Rank the class from 1(low) to 5(high) by its importance: ";
		if ((toReturn = readInteger(prompt)) < 1 || toReturn > 5){
			System.err.println("\nHomeworkPrioritizer: invalid input: not in range");
			return getClassImportance();
		} else {
			return toReturn;
		}
	}

	String getClassName() {
		String prompt = "Class Name: ";
		return readString(prompt);
	}

	int getCurrentAverage() throws NumberFormatException {
		int toReturn;
		String prompt = "Your current class average out of 100: ";
		if ((toReturn = readInteger(prompt)) < 0 || toReturn > 100){
			System.err.println("\nHomeworkPrioritizer: invalid input: not in range");
			return getCurrentAverage();
		} else {
			return toReturn;
		}
	}

	LocalDateTime getDueDate() throws DateTimeParseException {
		int count = 0;
		int maxTries = 5;
		while(true) {
			System.out.print("Assignment Due Date (format: yyyyy-MM-dd HH:mm AM/PM): ");
			try {
				return LocalDateTime.parse(sc.nextLine(), DateTimeFormatter.ofPattern("yyyy-MM-d hh:mm a"));
			} catch (DateTimeParseException ex) {
				if (++count == maxTries) {
					throw ex;
				} else {
					System.err.println("\nHomeworkPrioritizer: Bad Date/Time Formatting");
				}
			}
		}
	}

	int getGradePercent() throws NumberFormatException {
		int toReturn;
		String prompt = "Percent of this assignment toward final grade: ";
		if ((toReturn = readInteger(prompt)) < 0 || toReturn > 100){
			System.err.println("\nHomeworkPrioritizer: invalid input: not in range");
			return getGradePercent();
		} else {
			return toReturn;
		}
	}

	int getLatePenalty() throws NumberFormatException {
		int toReturn;
		String prompt = "Percent points lost per day late: ";
		if ((toReturn = readInteger(prompt)) < 0 || toReturn > 100){
			System.err.println("\nHomeworkPrioritizer: invalid input: not in range");
			return getLatePenalty();
		} else {
			return toReturn;
		}
	}

	int getOption() throws NumberFormatException {
		int toReturn;
		String prompt = " 1. Add a homework item\n"
					  + " 2. Remove a homework item\n"
					  + " 3. Clear all homework items\n"
					  + " 4. Import schedule from file\n"
					  + " 5. Save schedule to file\n"
					  + " 6. Print homework schedule to stdout\n"
					  + " 7. Exit\n"
					  + "Enter the option number: ";
		if ((toReturn = readInteger(prompt)) < 1 || toReturn > 7){
			System.err.println("\nHomeworkPrioritizer: invalid input: not in range");
			return getOption();
		} else {
			return toReturn;
		}
	}

	int getTimeToComplete() throws NumberFormatException {
		String prompt = "Approximate time in hours it will take to complete the assignment: ";
		return readInteger(prompt);
	}


	private int readInteger(String prompt) throws NumberFormatException {
		int count = 0;
		int maxTries = 3;

		while(true) {
			System.out.print(prompt);
			try {
				return Integer.parseInt(sc.nextLine());
			} catch (NumberFormatException ex) {
				if (++count == maxTries) {
					throw ex;
				} else {
					System.err.println("\nHomeworkPrioritizer: invalid input: expected integer");
				}
			}
		}
	}

	private String readString(String prompt) {
		String toReturn;
		do {
			System.out.print(prompt);
		} while ((toReturn = sc.nextLine()).equals(""));
		return toReturn;
	}


}