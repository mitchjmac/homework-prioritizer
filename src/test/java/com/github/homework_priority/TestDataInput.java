package com.github.homework_priority;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;


public class TestDataInput {

	private DataInput input;
	private InputStream testInput;

	/**
	 * Sets up the test fixture.
	 * (Called before every test case method.)
	 */
	@Before
	public void setUp() {}

	/**
	 * Tears down the test fixture.
	 * (Called after every test case method.)
	 */
	@After
	public void tearDown() throws IOException {
		testInput.close();
	}


	@Test
	public void testGetAssignmentName() {
		testInput = new ByteArrayInputStream(String.format("Project_2%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: Project_2", "Project_2", input.getAssignmentName());
	}
	@Test
	public void testGetClassImportance() {
		testInput = new ByteArrayInputStream(String.format("1%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: 1", 1, input.getClassImportance());
	}
	@Test
	public void testGetClassName() {
		testInput = new ByteArrayInputStream(String.format("Software Engineering%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: Software Engineering", "Software Engineering", input.getClassName());
	}
	@Test
	public void testGetCurrentAverage() {
		testInput = new ByteArrayInputStream(String.format("85%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: 85", 85, input.getCurrentAverage());
	}
	@Test
	public void testGetDueDate() {
		testInput = new ByteArrayInputStream(String.format("1988-10-28 06:42 AM%n").getBytes());
		input = new DataInput(testInput);
		LocalDateTime testDate = input.getDueDate();
		assertEquals("Date string-representation", "1988-10-28T06:42", testDate.toString());
	}
	@Test
	public void testGetGradePercent() {
		testInput = new ByteArrayInputStream(String.format("20%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: 20", 20, input.getGradePercent());
	}
	@Test
	public void testGetLatePenalty() {
		testInput = new ByteArrayInputStream(String.format("10%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: 10", 10, input.getLatePenalty());
	}
	@Test
	public void testGetOption() {
		testInput = new ByteArrayInputStream(String.format("3%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: 3", 3, input.getOption());
	}
	@Test
	public void testGetTimeToComplete() {
		testInput = new ByteArrayInputStream(String.format("3%n").getBytes());
		input = new DataInput(testInput);
		assertEquals("Should return same as input: 3", 3, input.getTimeToComplete());
	}


	@Test(expected=NumberFormatException.class)
	public void testGetClassImportance_exception() {
		testInput = new ByteArrayInputStream(String.format("not_an_int%nme_neither%nlol_nope%n").getBytes());
		input = new DataInput(testInput);
		input.getClassImportance();
	}
	@Test(expected=NumberFormatException.class)
	public void testGetCurrentAverage_exception() {
		testInput = new ByteArrayInputStream(String.format("not_an_int%nme_neither%nlol_nope%n").getBytes());
		input = new DataInput(testInput);
		input.getCurrentAverage();
	}
	@Test(expected=DateTimeParseException.class)
	public void testGetDueDate_exception() {
		testInput = new ByteArrayInputStream(String.format("jan 12%n1988-10-28 06:42AM%nyyyyy-MM-dd HH:mm AM%nstill_bad%n1988-10-28 6:42 AM%n").getBytes());
		input = new DataInput(testInput);
		input.getDueDate();
	}
	@Test(expected=NumberFormatException.class)
	public void testGetGradePercent_exception() {
		testInput = new ByteArrayInputStream(String.format("not_an_int%nme_neither%nlol_nope%n").getBytes());
		input = new DataInput(testInput);
		input.getGradePercent();
	}
	@Test(expected=NumberFormatException.class)
	public void testGetLatePenalty_exception() {
		testInput = new ByteArrayInputStream(String.format("not_an_int%nme_neither%nlol_nope%n").getBytes());
		input = new DataInput(testInput);
		input.getLatePenalty();
	}
	@Test(expected=NumberFormatException.class)
	public void testGetOption_exception() {
		testInput = new ByteArrayInputStream(String.format("not_an_int%nme_neither%nlol_nope%n").getBytes());
		input = new DataInput(testInput);
		input.getOption();
	}
	@Test(expected=NumberFormatException.class)
	public void testGetTimeToComplete_exception() {
		testInput = new ByteArrayInputStream(String.format("not_an_int%nme_neither%nlol_nope%n").getBytes());
		input = new DataInput(testInput);
		input.getTimeToComplete();
	}

}
