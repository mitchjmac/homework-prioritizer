package com.github.homework_priority;

import org.junit.*;
import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;


public class TestHomeworkAssignment {

	private HomeworkAssignment assignment;
	private LocalDateTime date;

	/**
	 * Sets up the test fixture.
	 * (Called before every test case method.)
	 */
	@Before
	public void setUp() {
		date = LocalDateTime.of(2017, 11, 15, 12, 16);
		assignment = new HomeworkAssignment("HW2", "SW Engineering", date, 5, 10, 5, 2, 80);
	}

	/**
	 * Tears down the test fixture.
	 * (Called after every test case method.)
	 */
	@After
	public void tearDown() {
		assignment = null;
	}

	@Test
	public void testToString() {
		assertEquals("Testing overriden toString method",
			String.format("%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n", "HW2", "SW Engineering", date, 5, 10, 5, 2, 80),
			 assignment.toString());
	}

}
